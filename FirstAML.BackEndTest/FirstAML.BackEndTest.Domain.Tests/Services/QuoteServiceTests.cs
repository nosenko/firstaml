﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstAML.BackEndTest.Domain.Services;
using FirstAML.BackEndTest.Domain.Services.Contracts.Quote;
using Moq;
using NUnit.Framework;

namespace FirstAML.BackEndTest.Domain.Tests.Services
{
	public class QuoteServiceTests
	{
		private Mock<IMatchingPackageService> _matchingPackageService;
		private Mock<IPackageSelectionService> _packageSelectionService;
		private Mock<IQuoteTotalCalculationService> _quoteTotalCalculationService;
		private Mock<ISpecialServiceService> _specialServiceService;
		private Mock<IPackageOverWeightFeeCalculator> _packageOverWeightFeeCalculator;

		[SetUp] 
		public void Setup()
		{
			_matchingPackageService = new Mock<IMatchingPackageService>();
			_packageSelectionService = new Mock<IPackageSelectionService>();
			_quoteTotalCalculationService = new Mock<IQuoteTotalCalculationService>();
			_specialServiceService = new Mock<ISpecialServiceService>();
			_packageOverWeightFeeCalculator = new Mock<IPackageOverWeightFeeCalculator>();
		}

		[Test]
		public void QuoteService_GetQuote_ThrowsNullRequest()
		{
			var service = GetDefaultQuoteService();
			Assert.Throws<ArgumentNullException>(() => service.GetQuote(null));
		}

		[Test]
		public void QuoteService_GetQuote_ThrowsNullRequestParcel()
		{
			var service = GetDefaultQuoteService();
			Assert.Throws<ArgumentNullException>(() => service.GetQuote(new Domain.Services.Contracts.QuoteServiceRequest 
				{ 
					Parcels = null
				})
			);
		}

		[Test]
		public void QuoteService_GetQuote_EmptyWhenNoParcels()
		{
			var serivce = GetDefaultQuoteService();
			var result = serivce.GetQuote(new Domain.Services.Contracts.QuoteServiceRequest
			{
				Parcels = new List<Parcel>()
			});

			Assert.IsNotNull(result);
			Assert.IsNotNull(result.Packages);
			Assert.IsFalse(result.Packages.Any());
			Assert.AreEqual(result.TotalCost, 0);
		}

		private IQuoteService GetDefaultQuoteService()
		{
			return new QuoteService(_matchingPackageService.Object, 
				_packageSelectionService.Object, 
				_quoteTotalCalculationService.Object, 
				_specialServiceService.Object,
				_packageOverWeightFeeCalculator.Object);
		}
	}
}

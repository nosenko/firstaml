﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FirstAML.BackEndTest.Domain.Services;
using FirstAML.BackEndTest.Domain.Services.Contracts.Quote;
using NUnit.Framework;

namespace FirstAML.BackEndTest.Domain.Tests.Services
{
	public class SpecialServicePriceCalculatorTests
	{
		[Test]
		public void SpecialServicePriceCalculator_GetTotal_ThrowsNullPackages()
		{
			var service = GetService();
			Assert.Throws<ArgumentNullException>(() => service.GetTotal(null, 1));
		}

		[Test]
		public void SpecialServicePriceCalculator_GetTotal_ThrowsNegativeFactor()
		{
			var service = GetService();
			Assert.Throws<ArgumentOutOfRangeException>(() => service.GetTotal(Enumerable.Empty<Package>(), -1));
		}

		[Test]
		public void SpecialServicePriceCalculator_GetTotal_SingleReturnsResult()
		{
			var service = GetService();
			
			var result = service.GetTotal(new List<Package> 
				{ 
					new Package
					{
						Cost = 5
					}
				}, 
				1
			);

			Assert.AreEqual(result, 5);
		}


		[Test]
		public void SpecialServicePriceCalculator_GetTotal_SumReturnsResult()
		{
			var service = GetService();

			var result = service.GetTotal(new List<Package>
				{
					new Package
					{
						Cost = 5
					},
					new Package
					{
						Cost = 5
					}
				},
				1
			);

			Assert.AreEqual(result, 10);
		}

		private ISpecialServicePriceCalculator GetService()
		{
			return new SpecialServicePriceCalculator();
		}
	}
}

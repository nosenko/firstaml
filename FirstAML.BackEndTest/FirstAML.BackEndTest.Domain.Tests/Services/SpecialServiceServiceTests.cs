﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FirstAML.BackEndTest.Domain.Exceptions;
using FirstAML.BackEndTest.Domain.Repositories.Service;
using FirstAML.BackEndTest.Domain.Services;
using FirstAML.BackEndTest.Domain.Services.Contracts.Quote;
using Moq;
using NUnit.Framework;

namespace FirstAML.BackEndTest.Domain.Tests.Services
{
	public class SpecialServiceServiceTests
	{
		private Mock<ISpecialServicePriceCalculator> _specialServicePriceCalculator;
		private Mock<ISpecialServiceRepository> _specialServiceRepository;

		[SetUp]
		public void Setup()
		{
			_specialServicePriceCalculator = new Mock<ISpecialServicePriceCalculator>();
			_specialServiceRepository = new Mock<ISpecialServiceRepository>();
		}

		[Test]
		public void SpecialServiceService_GetSpecialService_ThrowsNullSpecialService()
		{
			var service = GetService();
			Assert.Throws<ArgumentNullException>(() => service.GetSpecialService(null, Enumerable.Empty<Package>()));
		}

		[Test]
		public void SpecialServiceService_GetSpecialService_ThrowsNullPackages()
		{
			var service = GetService();
			Assert.Throws<ArgumentNullException>(() => service.GetSpecialService(new SpecialServiceRequest(), null));
		}

		[Test]
		public void SpecialServiceService_GetSpecialService_ThrowsNullSpecialServiceFromRepo()
		{
			_specialServiceRepository.Setup(r => r.GetById(It.IsAny<int>())).Returns<Models.Service.SpecialService>(null);

			var service = GetService();

			Assert.Throws<NoSpecialServiceFoundException>(() => service.GetSpecialService(new SpecialServiceRequest(), Enumerable.Empty<Package>()));
		}

		private ISpecialServiceService GetService()
		{
			return new SpecialServiceService(_specialServiceRepository.Object , _specialServicePriceCalculator.Object);
		}
	}
}

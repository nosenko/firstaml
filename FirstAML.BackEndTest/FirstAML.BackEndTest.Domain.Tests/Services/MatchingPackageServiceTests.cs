﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstAML.BackEndTest.Domain.Exceptions;
using FirstAML.BackEndTest.Domain.Models.Packing;
using FirstAML.BackEndTest.Domain.Repositories.Packing;
using FirstAML.BackEndTest.Domain.Services;
using FirstAML.BackEndTest.Domain.Services.Contracts.Quote;
using Moq;
using NUnit.Framework;

namespace FirstAML.BackEndTest.Domain.Tests.Services
{
	public class MatchingPackageServiceTests
	{
		private Mock<IPackageRepository> _packageRepository;
		private Mock<IPackageTypeService> _packageTypeService;

		[SetUp]
		public void Setup()
		{
			_packageRepository = new Mock<IPackageRepository>();
			_packageTypeService = new Mock<IPackageTypeService>();
		}

		[Test]
		public void MatchingPackageService_GetMatchingPackageForParcel_ThrowsNullParcel()
		{
			var service = GetPackageService();
			Assert.Throws<ArgumentNullException>(() => service.GetMatchingPackagesForParcel(null));
		}

		[Test]
		public void MatchingPackageService_GetMatchingPackageForParcel_ThrowsWhenNulloPackageTypes()
		{
			var service = GetPackageService();
			_packageTypeService.Setup(ps => ps.GetMatchingPackageTypesForParcel(It.IsAny<Parcel>())).Returns<Parcel>(null);

			Assert.Throws<NoPackageFoundException>(() => service.GetMatchingPackagesForParcel(new Parcel
			{
				BreadthMm = 50,
				HeightMm = 50,
				LengthMm = 50,
			}));
		}

		[Test]
		public void MatchingPackageService_GetMatchingPackageForParcel_ThrowsWhenEmptyPackageTypes()
		{
			var service = GetPackageService();
			_packageTypeService.Setup(ps => ps.GetMatchingPackageTypesForParcel(It.IsAny<Parcel>())).Returns(Enumerable.Empty<PackageType>());

			Assert.Throws<NoPackageFoundException>(() => service.GetMatchingPackagesForParcel(new Parcel
			{
				BreadthMm = 50,
				HeightMm = 50,
				LengthMm = 50,
			}));
		}

		[Test]
		public void MatchingPackageService_GetMatchingPackageForParcel_ThrowsWhenNullPackages()
		{
			var service = GetPackageService();
			SetupDefatulPackageTypeService();

			_packageRepository.Setup(pr => pr.GetAll())
				.Returns<IEnumerable<Models.Packing.Package>>(null);

			Assert.Throws<NoPackageFoundException>(() => service.GetMatchingPackagesForParcel(new Parcel
			{
				BreadthMm = 50,
				HeightMm = 50,
				LengthMm = 50,
			}));
		}

		[Test]
		public void MatchingPackageService_GetMatchingPackageForParcel_ThrowsWhenEmptyPackages()
		{
			var service = GetPackageService();
			SetupDefatulPackageTypeService();

			_packageRepository.Setup(pr => pr.GetAll())
				.Returns(Enumerable.Empty<Models.Packing.Package>());

			Assert.Throws<NoPackageFoundException>(() => service.GetMatchingPackagesForParcel(new Parcel
			{
				BreadthMm = 50,
				HeightMm = 50,
				LengthMm = 50,
			}));
		}

		[Test]
		public void MatchingPackageService_GetMatchingPackageForParcel_ResultWhentMatch()
		{
			var service = GetPackageService();
			SetupDefatulPackageTypeService();

			_packageRepository.Setup(pr => pr.GetAll())
				.Returns(new List<Models.Packing.Package>
					{
						new Models.Packing.Package
						{
							Id = 2,
							PackageTypeId = 2,
							Cost = 2
						}
					}
				);

			var result = service.GetMatchingPackagesForParcel(new Parcel
			{
				BreadthMm = 50,
				HeightMm = 50,
				LengthMm = 50,
			});

			Assert.IsNotNull(result);
			Assert.AreEqual(result.Count(), 1);
		}

		[Test]
		public void MatchingPackageService_GetMatchingPackageForParcel_ResultsWhenManyMatch()
		{
			var service = GetPackageService();
			SetupDefatulPackageTypeService();

			_packageRepository.Setup(pr => pr.GetAll())
				.Returns(new List<Models.Packing.Package>
					{
						new Models.Packing.Package
						{
							Id = 2,
							PackageTypeId = 2,
							Cost = 2
						},
						new Models.Packing.Package
						{
							Id = 1,
							PackageTypeId = 1,
							Cost = 1
						},
					}
				);

			var result = service.GetMatchingPackagesForParcel(new Parcel
			{
				BreadthMm = 50,
				HeightMm = 50,
				LengthMm = 50,
			});

			Assert.IsNotNull(result);
			Assert.AreEqual(result.Count(), 2);
		}

		private void SetupDefatulPackageTypeService()
		{
			_packageTypeService.Setup(ps => ps.GetMatchingPackageTypesForParcel(It.IsAny<Parcel>()))
				.Returns(new List<PackageType>
					{
						new PackageType
						{
							Id = 1,
							Dimensions = new Dimensions
							{
								BreadthMm = 100,
								HeightMm = 100,
								LengthMm = 100
							}
						},
						new PackageType
						{
							Id = 2,
							Dimensions = new Dimensions
							{
								BreadthMm = 200,
								HeightMm = 200,
								LengthMm = 200
							}
						}
					}
				);
		}

		private IMatchingPackageService GetPackageService()
		{
			return new MatchingPackageService(_packageRepository.Object, _packageTypeService.Object);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstAML.BackEndTest.Domain.Models.Packing;
using FirstAML.BackEndTest.Domain.Repositories.Packing;
using FirstAML.BackEndTest.Domain.Services;
using Moq;
using NUnit.Framework;
namespace FirstAML.BackEndTest.Domain.Tests.Services
{
	public class PackageTypeServiceTests
	{
		private Mock<IPackageTypeRepository> _packageTypeRepository;

		[SetUp]
		public void Setup()
		{
			_packageTypeRepository = new Mock<IPackageTypeRepository>();
		}

		[Test]
		public void PackageTypeService_GetMatchingPackageTypesForParcel_ThrowsNullParcel()
		{
			var service = GetPackageTypeService();
			Assert.Throws<ArgumentNullException>(() => service.GetMatchingPackageTypesForParcel(null));
		}

		[Test]
		public void PackageTypeService_GetMatchingPackageTypesForParcel_GetsNoMatchingPackageType()
		{
			var service = GetPackageTypeService();
			SetupDefaultPackageTypeRepo();

			var results = service.GetMatchingPackageTypesForParcel(new Domain.Services.Contracts.Quote.Parcel
			{
				BreadthMm = 300,
				HeightMm = 300,
				LengthMm = 300
			});

			Assert.IsNotNull(results);
			Assert.IsFalse(results.Any());
		}

		[Test]
		public void PackageTypeService_GetMatchingPackageTypesForParcel_GetsMatchingPackageType()
		{
			var service = GetPackageTypeService();
			SetupDefaultPackageTypeRepo();

			var results = service.GetMatchingPackageTypesForParcel(new Domain.Services.Contracts.Quote.Parcel
			{
				BreadthMm = 150,
				HeightMm = 150,
				LengthMm = 150
			});

			Assert.IsNotNull(results);
			Assert.IsTrue(results.Any());
			Assert.AreEqual(results.Count(), 1);
			Assert.AreEqual(results.First().Id, 2);
		}

		[Test]
		public void PackageTypeService_GetMatchingPackageTypesForParcel_GetsMatchingPackageTypes()
		{
			var service = GetPackageTypeService();
			SetupDefaultPackageTypeRepo();

			var results = service.GetMatchingPackageTypesForParcel(new Domain.Services.Contracts.Quote.Parcel
			{
				BreadthMm = 50,
				HeightMm = 50,
				LengthMm = 50
			});

			Assert.IsNotNull(results);
			Assert.IsTrue(results.Any());
			Assert.AreEqual(results.Count(), 2);
		}

		private void SetupDefaultPackageTypeRepo()
		{
			_packageTypeRepository.Setup(repo => repo.GetAll())
				.Returns(new List<PackageType>
				{
					new PackageType
					{
						Id = 1,
						Dimensions = new Dimensions
						{
							BreadthMm = 100,
							HeightMm = 100,
							LengthMm = 100
						}
					},
					new PackageType
					{
						Id = 2,
						Dimensions = new Dimensions
						{
							BreadthMm = 200,
							HeightMm = 200,
							LengthMm = 200
						}
					}
				}
			);
		}

		private IPackageTypeService GetPackageTypeService()
		{
			return new PackageTypeService(_packageTypeRepository.Object);
		}
	}
}

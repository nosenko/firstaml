﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FirstAML.BackEndTest.Domain.Models.Fees;
using FirstAML.BackEndTest.Domain.Repositories.Fees;
using FirstAML.BackEndTest.Domain.Services;
using Moq;
using NUnit.Framework;

namespace FirstAML.BackEndTest.Domain.Tests.Services
{
	public class PackageOverWeightFeeCalculatorTests
	{
		private Mock<IPackageOverWeightFeeRepository> _packageOverWeightFeeRepository;

		[SetUp]
		public void Setup()
		{
			_packageOverWeightFeeRepository = new Mock<IPackageOverWeightFeeRepository>();
		}

		[Test]
		public void PackageOverWeightFeeCalculator_GetOverWeightFee_ThrowsNullPackage()
		{
			var service = GetService();
			Assert.Throws<ArgumentNullException>(() => service.GetOverWeightFee(1, null));
		}

		[Test]
		public void PackageOverWeightFeeCalculator_GetOverWeightFee_ThrowsNegativeFee()
		{
			var service = GetService();
			Assert.Throws<ArgumentOutOfRangeException>(() => service.GetOverWeightFee(-1, new Domain.Services.Contracts.Quote.Package()));
		}

		[Test]
		public void PackageOverWeightFeeCalculator_GetOverWeightFee_ZeroWhenUnderWeight()
		{
			var service = GetService();
			var result = service.GetOverWeightFee(
				1, 
				new Domain.Services.Contracts.Quote.Package
				{
					FreeWeightQuotaKg = 2
				}
			);

			Assert.AreEqual(result, 0);
		}

		[Test]
		public void PackageOverWeightFeeCalculator_GetOverWeightFee_ZeroWhenNoFee()
		{
			_packageOverWeightFeeRepository.Setup(r => r.GetFeeForPackage(It.IsAny<int>())).Returns<OverWeightFee>(null);

			var service = GetService();
			var result = service.GetOverWeightFee(
				4,
				new Domain.Services.Contracts.Quote.Package
				{
					FreeWeightQuotaKg = 2
				}
			);

			Assert.AreEqual(result, 0);
		}

		[Test]
		public void PackageOverWeightFeeCalculator_GetOverWeightFee_ZeroWhenFeeNoCharge()
		{
			_packageOverWeightFeeRepository.Setup(r => r.GetFeeForPackage(It.IsAny<int>()))
				.Returns(new OverWeightFee 
					{ 
						ChargePerKg = null
					}
				);

			var service = GetService();
			var result = service.GetOverWeightFee(
				4,
				new Domain.Services.Contracts.Quote.Package
				{
					FreeWeightQuotaKg = 2
				}
			);

			Assert.AreEqual(result, 0);
		}

		[Test]
		public void PackageOverWeightFeeCalculator_GetOverWeightFee_FeeWhenApplicable()
		{
			_packageOverWeightFeeRepository.Setup(r => r.GetFeeForPackage(It.IsAny<int>()))
				.Returns(new OverWeightFee
				{
					ChargePerKg = 2
				}
			);

			var service = GetService();
			var result = service.GetOverWeightFee(
				4,
				new Domain.Services.Contracts.Quote.Package
				{
					FreeWeightQuotaKg = 2
				}
			);

			Assert.AreEqual(result, 4);
		}

		private IPackageOverWeightFeeCalculator GetService()
		{
			return new PackageOverWeightFeeCalculator(_packageOverWeightFeeRepository.Object);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstAML.BackEndTest.Domain.Services;
using FirstAML.BackEndTest.Domain.Services.Contracts.Quote;
using NUnit.Framework;

namespace FirstAML.BackEndTest.Domain.Tests.Services
{
	public class QuoteTotalCalculationServiceTests
	{
		[Test]
		public void QuoteTotalCalculationService_GetQuoteTotal_ThrowsNullPackages()
		{
			var service = GetService();
			Assert.Throws<ArgumentNullException>(() => service.GetQuoteTotal(null, Enumerable.Empty<SpecialService>()));
		}

		[Test]
		public void QuoteTotalCalculationService_GetQuoteTotal_ThrowsNullSpecialServices()
		{
			var service = GetService();
			Assert.Throws<ArgumentNullException>(() => service.GetQuoteTotal(Enumerable.Empty<Package>(), null));
		}

		[Test]
		public void QuoteTotalCalculationService_GetQuoteTotal_ZeroNoLines()
		{
			var service = GetService();
			var result = service.GetQuoteTotal(Enumerable.Empty<Package>(), Enumerable.Empty<SpecialService>());

			Assert.AreEqual(result, 0m);
		}

		[Test]
		public void QuoteTotalCalculationService_GetQuoteTotal_SumOfLines()
		{
			var service = GetService();
			var result = service.GetQuoteTotal(new List<Package>
				{
					new Package
					{
						Cost = 5m
					},
					new Package
					{
						Cost = 2.5m
					}
				},
				Enumerable.Empty<SpecialService>()
			);

			Assert.AreEqual(result, 7.5m);
		}

		[Test]
		public void QuoteTotalCalculationService_GetQuoteTotal_SumOfServices()
		{
			var service = GetService();
			var result = service.GetQuoteTotal(
				Enumerable.Empty<Package>(),
				new List<SpecialService>
				{
					new SpecialService
					{
						Cost = 5
					},
					new SpecialService
					{
						Cost = 2.5m
					}
				}
			);

			Assert.AreEqual(result, 7.5m);
		}

		[Test]
		public void QuoteTotalCalculationService_GetQuoteTotal_SumOfLinesAndServices()
		{
			var service = GetService();
			var result = service.GetQuoteTotal(new List<Package>
				{
					new Package
					{
						Cost = 5m
					},
					new Package
					{
						Cost = 2.5m
					}
				},
				new List<SpecialService>
				{
					new SpecialService
					{
						Cost = 5
					},
					new SpecialService
					{
						Cost = 2.5m
					}
				}
			);

			Assert.AreEqual(result, 15);
		}

		private IQuoteTotalCalculationService GetService()
		{
			return new QuoteTotalCalculationService();
		}
	}
}

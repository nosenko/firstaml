﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstAML.BackEndTest.Domain.Services;
using FirstAML.BackEndTest.Domain.Services.Contracts.Quote;
using NUnit.Framework;

namespace FirstAML.BackEndTest.Domain.Tests.Services
{
	public class PackageSelectionServiceTests
	{
		[Test]
		public void PackageSelectionService_ChoosePackage_ThrowsNullPackages()
		{
			var service = GetPackageService();
			Assert.Throws<ArgumentNullException>(() => service.GetCheapestPackage(null));
		}

		[Test]
		public void PackageSelectionService_ChoosePackage_NullWhenNullPackages()
		{
			var service = GetPackageService();
			var result = service.GetCheapestPackage(Enumerable.Empty<Package>());

			Assert.IsNull(result);
		}

		[Test]
		public void PackageSelectionService_ChoosePackage_GetsCheapestCostPackage()
		{
			var service = GetPackageService();
			var result = service.GetCheapestPackage(new List<Package> 
				{ 
					new Package
					{
						Cost = 4
					}, 
					new Package
					{
						Cost = 2
					},
					new Package
					{
						Cost = 3
					}
				}
			);

			Assert.IsNotNull(result);
			Assert.AreEqual(result.TotalCost, 2);
		}

		private IPackageSelectionService GetPackageService()
		{
			return new PackageSelectionService();
		}
	}
}

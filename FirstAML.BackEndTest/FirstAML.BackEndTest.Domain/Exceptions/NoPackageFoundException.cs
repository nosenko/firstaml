﻿using System;
using FirstAML.BackEndTest.Domain.Services.Contracts.Quote;

namespace FirstAML.BackEndTest.Domain.Exceptions
{
	public class NoPackageFoundException : Exception
	{
		public Parcel Parcel { get; set; }
	}
}

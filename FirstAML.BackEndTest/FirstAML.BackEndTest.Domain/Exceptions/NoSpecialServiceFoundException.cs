﻿using System;

namespace FirstAML.BackEndTest.Domain.Exceptions
{
	public class NoSpecialServiceFoundException : Exception
	{
		public int SpecialServiceId { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstAML.BackEndTest.Domain.Exceptions;
using FirstAML.BackEndTest.Domain.Repositories.Packing;
using FirstAML.BackEndTest.Domain.Services.Contracts.Quote;

namespace FirstAML.BackEndTest.Domain.Services
{
	public interface IMatchingPackageService
	{
		IEnumerable<Package> GetMatchingPackagesForParcel(Parcel parcel);
	}

	public class MatchingPackageService : IMatchingPackageService
	{
		private readonly IPackageRepository _packageRepository;
		private readonly IPackageTypeService _packageTypeService;

		public MatchingPackageService(
			IPackageRepository packageRepository,
			IPackageTypeService packageTypeService)
		{
			_packageRepository = packageRepository;
			_packageTypeService = packageTypeService;
		}

		public IEnumerable<Package> GetMatchingPackagesForParcel(Parcel parcel)
		{
			_ = parcel ?? throw new ArgumentNullException(nameof(parcel));

			var packageTypes = _packageTypeService.GetMatchingPackageTypesForParcel(parcel)?
				.ToDictionary(pt => pt.Id, pt => pt);

			if (packageTypes == null || !packageTypes.Any())
			{
				throw new NoPackageFoundException
				{
					Parcel = parcel
				};
			}

			var packages = _packageRepository.GetAll()?
				.Where(p => packageTypes.ContainsKey(p.PackageTypeId));

			if (packages == null || !packages.Any())
			{
				throw new NoPackageFoundException
				{
					Parcel = parcel
				};
			}

			return packages.Select(p => new Package
			{
				Id = p.Id,
				PackageTypeName = packageTypes[p.PackageTypeId].Name,
				FreeWeightQuotaKg = p.FreeWeightQuotaKg,
				Cost = p.Cost
			});
		}
	}
}

﻿using System;
using FirstAML.BackEndTest.Domain.Repositories.Fees;

namespace FirstAML.BackEndTest.Domain.Services
{
	public interface IPackageOverWeightFeeCalculator
	{
		decimal GetOverWeightFee(decimal weightKg, Contracts.Quote.Package package);
	}

	public class PackageOverWeightFeeCalculator : IPackageOverWeightFeeCalculator
	{
		private readonly IPackageOverWeightFeeRepository _packageOverWeightFeeRepository;

		public PackageOverWeightFeeCalculator(IPackageOverWeightFeeRepository packageOverWeightFeeRepository)
		{
			_packageOverWeightFeeRepository = packageOverWeightFeeRepository;
		}

		public decimal GetOverWeightFee(decimal weightKg, Contracts.Quote.Package package)
		{
			_ = package ?? throw new ArgumentNullException(nameof(package));
			
			if (weightKg < 0)
			{
				throw new ArgumentOutOfRangeException(nameof(weightKg));
			}

			if (package.FreeWeightQuotaKg >= weightKg)
			{
				return 0;
			}

			var fee = _packageOverWeightFeeRepository.GetFeeForPackage(package.Id);

			if (fee == null || !fee.ChargePerKg.HasValue)
			{
				return 0;
			}

			return (weightKg - package.FreeWeightQuotaKg) * fee.ChargePerKg.Value;
		}
	}
}

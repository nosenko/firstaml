﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstAML.BackEndTest.Domain.Services.Contracts.Quote;

namespace FirstAML.BackEndTest.Domain.Services
{
	public interface ISpecialServicePriceCalculator
	{
		decimal GetTotal(IEnumerable<Contracts.Quote.Package> packages, decimal costFactor);
	}

	public class SpecialServicePriceCalculator : ISpecialServicePriceCalculator
	{
		public decimal GetTotal(IEnumerable<Package> packages, decimal costFactor)
		{
			_ = packages ?? throw new ArgumentNullException(nameof(packages));

			if (costFactor < 0)
			{
				throw new ArgumentOutOfRangeException(nameof(costFactor));
			}

			return packages.Sum(p => p.TotalCost) * costFactor;
		}
	}
}

﻿using System;
using System.Linq;
using FirstAML.BackEndTest.Domain.Services.Contracts;

namespace FirstAML.BackEndTest.Domain.Services
{
	public interface IQuoteService
	{
		QuoteServiceResponse GetQuote(QuoteServiceRequest request);
	}

	public class QuoteService : IQuoteService
	{
		private readonly IMatchingPackageService _matchingPackageService;
		private readonly IPackageSelectionService _packageSelectionService;
		private readonly IQuoteTotalCalculationService _quoteTotalCalculationService;
		private readonly ISpecialServiceService _specialServiceService;
		private readonly IPackageOverWeightFeeCalculator _packageOverWeightFeeCalculator;

		public QuoteService(
			IMatchingPackageService matchingPackageService,
			IPackageSelectionService packageSelectionService,
			IQuoteTotalCalculationService quoteTotalCalculationService,
			ISpecialServiceService specialServiceService,
			IPackageOverWeightFeeCalculator packageOverWeightFeeCalculator
		)
		{
			_matchingPackageService = matchingPackageService;
			_packageSelectionService = packageSelectionService;
			_quoteTotalCalculationService = quoteTotalCalculationService;
			_specialServiceService = specialServiceService;
			_packageOverWeightFeeCalculator = packageOverWeightFeeCalculator;
		}

		public QuoteServiceResponse GetQuote(QuoteServiceRequest request)
		{
			_ = request ?? throw new ArgumentNullException(nameof(request));
			_ = request.Parcels ?? throw new ArgumentNullException(nameof(request.Parcels));

			var result = new QuoteServiceResponse();

			if (!request.Parcels.Any())
			{
				return result;
			}

			foreach(var parcel in request.Parcels)
			{
				var packages = _matchingPackageService.GetMatchingPackagesForParcel(parcel);
				
				foreach(var package in packages)
				{
					package.OverweightFee = _packageOverWeightFeeCalculator.GetOverWeightFee(parcel.WeightKg, package);
				}

				var cheapestPackage = _packageSelectionService.GetCheapestPackage(packages);

				result.Packages.Add(cheapestPackage);
			}

			foreach(var ssr in request.SpecialServiceRequests)
			{
				var specialService = _specialServiceService.GetSpecialService(ssr, result.Packages);

				result.SpecialServices.Add(specialService);
			}

			result.TotalCost = _quoteTotalCalculationService.GetQuoteTotal(result.Packages, result.SpecialServices);

			return result;
		}
	}
}

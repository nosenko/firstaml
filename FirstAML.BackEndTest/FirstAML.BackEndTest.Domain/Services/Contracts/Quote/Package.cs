﻿namespace FirstAML.BackEndTest.Domain.Services.Contracts.Quote
{
	public class Package
	{
		public int Id { get; set; }
		public string PackageTypeName { get; set; }
		public decimal Cost { get; set; }
		public decimal FreeWeightQuotaKg { get; set; }
		public decimal OverweightFee { get; set; }
		public decimal TotalCost => Cost + OverweightFee;
	}
}

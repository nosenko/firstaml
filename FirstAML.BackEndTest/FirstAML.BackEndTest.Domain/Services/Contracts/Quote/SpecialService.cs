﻿namespace FirstAML.BackEndTest.Domain.Services.Contracts.Quote
{
	public class SpecialService
	{
		public int Id { get; set; }
		public string ServiceName { get; set; }
		public decimal Cost { get; set; }
	}
}

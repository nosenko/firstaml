﻿namespace FirstAML.BackEndTest.Domain.Services.Contracts.Quote
{
	public class Parcel
	{
		public int LengthMm { get; set; }
		public int HeightMm { get; set; }
		public int BreadthMm { get; set; }
		public decimal WeightKg { get; set; }
	}
}

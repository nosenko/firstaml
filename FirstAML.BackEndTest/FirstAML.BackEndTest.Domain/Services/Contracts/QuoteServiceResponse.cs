﻿using System.Collections.Generic;

namespace FirstAML.BackEndTest.Domain.Services.Contracts
{
	public class QuoteServiceResponse
	{
		public List<Quote.Package> Packages { get; } = new List<Quote.Package>();
		public List<Quote.SpecialService> SpecialServices { get; } = new List<Quote.SpecialService>();
		public decimal TotalCost { get; set; }
	}
}

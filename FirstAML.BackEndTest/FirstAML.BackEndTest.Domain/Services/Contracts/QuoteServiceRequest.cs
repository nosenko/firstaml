﻿using System.Collections.Generic;
using FirstAML.BackEndTest.Domain.Services.Contracts.Quote;

namespace FirstAML.BackEndTest.Domain.Services.Contracts
{
	public class QuoteServiceRequest
	{
		public IEnumerable<Parcel> Parcels { get; set; }
		public IEnumerable<SpecialServiceRequest> SpecialServiceRequests { get; set; }
	}
}

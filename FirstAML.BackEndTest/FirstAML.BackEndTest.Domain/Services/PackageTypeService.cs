﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstAML.BackEndTest.Domain.Models.Packing;
using FirstAML.BackEndTest.Domain.Repositories.Packing;
using FirstAML.BackEndTest.Domain.Services.Contracts.Quote;

namespace FirstAML.BackEndTest.Domain.Services
{
	public interface IPackageTypeService
	{
		IEnumerable<PackageType> GetMatchingPackageTypesForParcel(Parcel parcel);
	}

	public class PackageTypeService : IPackageTypeService
	{
		private readonly IPackageTypeRepository _packageTypeRepository;

		public PackageTypeService(IPackageTypeRepository packageTypeRepository)
		{
			_packageTypeRepository = packageTypeRepository;
		}

		public IEnumerable<PackageType> GetMatchingPackageTypesForParcel(Parcel parcel)
		{
			_ = parcel ?? throw new ArgumentNullException(nameof(parcel));

			return _packageTypeRepository.GetAll()
				.Where(pt =>
					parcel.BreadthMm < pt.Dimensions.BreadthMm
					&& parcel.LengthMm < pt.Dimensions.LengthMm
					&& parcel.HeightMm < pt.Dimensions.HeightMm
				)
				.ToList();
		}
	}
}

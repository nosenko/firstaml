﻿using System;
using System.Collections.Generic;
using FirstAML.BackEndTest.Domain.Exceptions;
using FirstAML.BackEndTest.Domain.Repositories.Service;
using FirstAML.BackEndTest.Domain.Services.Contracts.Quote;

namespace FirstAML.BackEndTest.Domain.Services
{
	public interface ISpecialServiceService
	{
		SpecialService GetSpecialService(SpecialServiceRequest specialServiceRequest, IEnumerable<Package> packages);
	}

	public class SpecialServiceService : ISpecialServiceService
	{
		private readonly ISpecialServiceRepository _specialServiceRepository;
		private readonly ISpecialServicePriceCalculator _specialServicePriceCalculator;

		public SpecialServiceService(
			ISpecialServiceRepository specialServiceRepository,
			ISpecialServicePriceCalculator specialServicePriceCalculator)
		{
			_specialServiceRepository = specialServiceRepository;
			_specialServicePriceCalculator = specialServicePriceCalculator;
		}


		public SpecialService GetSpecialService(SpecialServiceRequest specialServiceRequest, IEnumerable<Package> packages)
		{
			_ = specialServiceRequest ?? throw new ArgumentNullException(nameof(specialServiceRequest));
			_ = packages ?? throw new ArgumentNullException(nameof(packages));

			var specialService = _specialServiceRepository.GetById(specialServiceRequest.SpecialServiceId);

			if (specialService == null)
			{
				throw new NoSpecialServiceFoundException
				{
					SpecialServiceId = specialServiceRequest.SpecialServiceId
				};
			}

			var result = new SpecialService
			{
				Id = specialService.Id,
				ServiceName = specialService.Name,
				Cost = _specialServicePriceCalculator.GetTotal(packages, specialService.CostFactor)
			};

			return result;
		}
	}
}

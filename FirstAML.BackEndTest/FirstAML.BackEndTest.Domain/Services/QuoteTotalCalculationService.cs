﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstAML.BackEndTest.Domain.Services.Contracts.Quote;

namespace FirstAML.BackEndTest.Domain.Services
{
	public interface IQuoteTotalCalculationService
	{
		decimal GetQuoteTotal(IEnumerable<Package> packages, IEnumerable<SpecialService> specialServices);
	}

	public class QuoteTotalCalculationService : IQuoteTotalCalculationService
	{
		public decimal GetQuoteTotal(IEnumerable<Package> packages, IEnumerable<SpecialService> specialServices)
		{
			_ = packages ?? throw new ArgumentNullException(nameof(packages));
			_ = specialServices ?? throw new ArgumentNullException(nameof(specialServices));

			return packages.Sum(p => p.TotalCost) + specialServices.Sum(p => p.Cost);
		}
	}
}

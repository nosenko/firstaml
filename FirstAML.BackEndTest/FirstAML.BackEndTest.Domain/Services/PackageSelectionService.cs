﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstAML.BackEndTest.Domain.Services.Contracts.Quote;

namespace FirstAML.BackEndTest.Domain.Services
{
	public interface IPackageSelectionService
	{
		Package GetCheapestPackage(IEnumerable<Package> quotePackages);
	}

	public class PackageSelectionService : IPackageSelectionService
	{
		public Package GetCheapestPackage(IEnumerable<Package> packages)
		{
			_ = packages ?? throw new ArgumentNullException(nameof(packages));
			
			if (!packages.Any())
			{
				return null;
			}

			return packages.OrderBy(p => p.TotalCost).First();
		}
	}
}

﻿namespace FirstAML.BackEndTest.Domain.Models.Fees
{
	public class OverWeightFee
	{
		public int Id { get; set; }
		public int PackageId { get; set; }
		public decimal? ChargePerKg { get; set; }
	}
}

﻿namespace FirstAML.BackEndTest.Domain.Models.Service
{
	public class SpecialService
	{
		public int Id { get; set; }
		public decimal CostFactor { get; set; }
		public string Name { get; set; }
	}
}

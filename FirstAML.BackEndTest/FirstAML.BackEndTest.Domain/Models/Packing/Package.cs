﻿namespace FirstAML.BackEndTest.Domain.Models.Packing
{
	public class Package
	{
		public int Id { get; set; }
		public int PackageTypeId { get; set; }
		public decimal Cost { get; set; } 
		public decimal FreeWeightQuotaKg { get; set; }
	}
}

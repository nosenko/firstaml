﻿namespace FirstAML.BackEndTest.Domain.Models.Packing
{
	public class Dimensions
	{
		public int LengthMm { get; set; }
		public int HeightMm { get; set; }
		public int BreadthMm { get; set; }
	}
}

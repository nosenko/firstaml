﻿namespace FirstAML.BackEndTest.Domain.Models.Packing
{
	public class PackageType
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public Dimensions Dimensions { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirstAML.BackEndTest.Domain.Models.Packing;

namespace FirstAML.BackEndTest.Domain.Repositories.Packing
{
	public interface IPackageTypeRepository 
	{
		IEnumerable<PackageType> GetAll();
		PackageType GetById(int id);
	}

	public class PackageTypeRepository : IPackageTypeRepository
	{
		private IEnumerable<PackageType> _packageTypes = new List<PackageType>
		{
			new PackageType
			{
				Id = 1,
				Name = "Small",
				Dimensions = new Dimensions
				{
					BreadthMm = 100,
					HeightMm = 100,
					LengthMm = 100
				}
			},
			new PackageType
			{
				Id = 2,
				Name = "Medium",
				Dimensions = new Dimensions
				{
					BreadthMm = 500,
					HeightMm = 500,
					LengthMm = 500
				}
			},
			new PackageType
			{
				Id = 3,
				Name = "Large",
				Dimensions = new Dimensions
				{
					BreadthMm = 1000,
					HeightMm = 1000,
					LengthMm = 1000
				}
			},
			new PackageType
			{
				Id = 4,
				Name = "XL",
				Dimensions = new Dimensions
				{
					BreadthMm = int.MaxValue,
					HeightMm = int.MaxValue,
					LengthMm = int.MaxValue
				}
			},
			new PackageType
			{
				Id = 5,
				Name = "Heavy Weight",
				Dimensions = new Dimensions
				{
					BreadthMm = int.MaxValue,
					HeightMm = int.MaxValue,
					LengthMm = int.MaxValue
				}
			}

		};

		public IEnumerable<PackageType> GetAll() => _packageTypes;

		public PackageType GetById(int id) => _packageTypes.FirstOrDefault(pt => pt.Id == id);
	}
}

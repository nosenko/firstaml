﻿using System.Collections.Generic;
using FirstAML.BackEndTest.Domain.Models.Packing;

namespace FirstAML.BackEndTest.Domain.Repositories.Packing
{
	public interface IPackageRepository
	{
		IEnumerable<Package> GetAll();
	}

	public class PackageRepository : IPackageRepository
	{
		private IEnumerable<Package> _packages = new List<Package> 
		{ 
			new Package
			{
				Id = 1,
				PackageTypeId = 1,
				Cost = 3m,
				FreeWeightQuotaKg = 1m
			},
			new Package
			{
				Id = 2,
				PackageTypeId = 2,
				Cost = 8m,
				FreeWeightQuotaKg = 3m
			},
			new Package
			{
				Id = 3,
				PackageTypeId = 3,
				Cost = 15m,
				FreeWeightQuotaKg = 6m
			},
			new Package
			{
				Id = 4,
				PackageTypeId = 4,
				Cost = 25m,
				FreeWeightQuotaKg = 10m
			},
			new Package
			{
				Id = 5,
				PackageTypeId = 5,
				Cost = 50m,
				FreeWeightQuotaKg = 50m
			}
		};

		public IEnumerable<Package> GetAll() => _packages;
	}
}

﻿using System.Collections.Generic;
using System.Linq;
using FirstAML.BackEndTest.Domain.Models.Fees;

namespace FirstAML.BackEndTest.Domain.Repositories.Fees
{
	public interface IPackageOverWeightFeeRepository
	{
		public OverWeightFee GetFeeForPackage(int packageId);
	}

	public class PackageOverWeightFeeRepository : IPackageOverWeightFeeRepository
	{
		private readonly List<OverWeightFee> _overWeightFees = new List<OverWeightFee>
		{
			new OverWeightFee
			{
				Id = 1,
				PackageId = 1,
				ChargePerKg = 2
			},
			new OverWeightFee
			{
				Id = 2,
				PackageId = 2,
				ChargePerKg = 2
			},
			new OverWeightFee
			{
				Id = 3,
				PackageId = 3,
				ChargePerKg = 2
			},
			new OverWeightFee
			{
				Id = 4,
				PackageId = 4,
				ChargePerKg = 2
			},
			new OverWeightFee
			{
				Id = 5,
				PackageId = 5,
				ChargePerKg = 1
			}
		};

		public OverWeightFee GetFeeForPackage(int packageId)
		{
			return _overWeightFees.FirstOrDefault(f => packageId == f.PackageId);
		}
	}
}

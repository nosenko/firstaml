﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstAML.BackEndTest.Domain.Models.Service;

namespace FirstAML.BackEndTest.Domain.Repositories.Service
{
	public interface ISpecialServiceRepository
	{
		SpecialService GetById(int id);
	}

	public class SpecialServiceRepository : ISpecialServiceRepository
	{
		private readonly List<SpecialService> _specialServices = new List<SpecialService>
		{
			new SpecialService
			{
				Id = 1,
				CostFactor = 1,
				Name = "Speedy Shipping"
			}
		};

		public SpecialService GetById(int id)
		{
			return _specialServices.FirstOrDefault(ss => ss.Id == id);
		}
	}
}
